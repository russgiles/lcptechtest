﻿using System;
using System.Linq;
using LcpTechTest.Infrastructure.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LcpTechTest.API.IntegrationTests.Factories
{
	public class CustomWebApplicationFactory<TStartup>: WebApplicationFactory<TStartup> where TStartup : class
	{
		protected override void ConfigureWebHost(IWebHostBuilder builder)
		{
			builder.ConfigureServices(async services =>
			{
				var descriptor = services.SingleOrDefault(
					d => d.ServiceType ==
					     typeof(DbContextOptions<LcpContext>));

				services.Remove(descriptor);

				services.AddDbContext<LcpContext>(options =>
				{
					options.UseInMemoryDatabase("InMemoryDbForTesting");
				});

				var sp = services.BuildServiceProvider();

				using (var scope = sp.CreateScope())
				{
					var scopedServices = scope.ServiceProvider;
					var db = scopedServices.GetRequiredService<LcpContext>();
					var loggerFactory = scopedServices.GetRequiredService<ILoggerFactory>();

					try
					{
						await LcpContextSeed.SeedAsync(db, loggerFactory);
					}
					catch (Exception ex)
					{
						var logger = loggerFactory.CreateLogger<TStartup>();
						logger.LogError(ex, "An error occurred seeding the " +
						                    "database with test messages. Error: {Message}", ex.Message);
					}
				}
			});
		}
	}
}
