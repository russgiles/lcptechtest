using FluentAssertions;
using LcpTechTest.API.Dtos;
using LcpTechTest.API.IntegrationTests.Factories;
using LcpTechTest.API.IntegrationTests.Helpers;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace LcpTechTest.API.IntegrationTests
{
	public class ClientApiIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
	{
	    private readonly CustomWebApplicationFactory<Startup> _factory;
	    private readonly HttpClient _client;

	    public ClientApiIntegrationTests(CustomWebApplicationFactory<Startup> factory)
	    {
		    _factory = factory;
		    _client = _factory.CreateClient(new WebApplicationFactoryClientOptions());
	    }

		[Fact]
	    public async Task Clients_GivenDefaultSeedData_ShouldReturnSixItems()
	    {
		    // Arrange
		    const string getClientsUrl = "/api/clients/";

		    // Act
		    var response = await _client.GetAsync(getClientsUrl);
		    var responseString = await response.Content.ReadAsStringAsync();
		    var clients = JsonConvert.DeserializeObject<IReadOnlyList<ClientToReturnDto>>(responseString);

		    // Assert
		    response.EnsureSuccessStatusCode();
		    clients.Count.Should().Be(6);
	    }

	    [Fact]
	    public async Task Clients_CreatingAnItem_ShouldAddItToTheDatabase()
	    {
		    // Arrange
		    const string url = "/api/clients/";

		    // Act & Assert
		    var originalResponse = await _client.GetAsync(url);
		    var responseString = await originalResponse.Content.ReadAsStringAsync();
		    var originalClients = JsonConvert.DeserializeObject<IReadOnlyList<ClientToReturnDto>>(responseString);

		    originalResponse.EnsureSuccessStatusCode();
		    originalClients.Count.Should().Be(6);


		    var postBody = new
		    {
			    Name = "Test User",
			    CaseManagerId = 1,
			    ClientTypeId = 2
		    };

		    await _client.PostAsync(url, ContentHelper.GetStringContent(postBody));

		    var newResponse = await _client.GetAsync(url);
		    var newString = await newResponse.Content.ReadAsStringAsync();
		    var newClients = JsonConvert.DeserializeObject<IReadOnlyList<ClientToReturnDto>>(newString);

		    newResponse.EnsureSuccessStatusCode();
		    newClients.Count.Should().Be(7);
		    newClients.Should().Contain(x => x.Name == "Test User");
	    }
	}
}
