﻿using AutoMapper;
using FluentAssertions;
using LcpTechTest.API.Controllers;
using LcpTechTest.API.Dtos;
using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace LcpTechTest.API.UnitTests.Controllers
{
	public class ClientControllerTests
	{
		private readonly Mock<IClientService> _clientServiceMock;
		private readonly ClientsController _clientsController;

		public ClientControllerTests()
		{
			var mapperMock = new Mock<IMapper>();
			_clientServiceMock = new Mock<IClientService>();
			_clientsController = new ClientsController(_clientServiceMock.Object, mapperMock.Object);
		}

		[Fact]
		public async Task GetClientById_WhenClientDoesNotExist_ReturnsNotFound()
		{
			// Arrange
			const int testClientId = 1;
			_clientServiceMock.Setup(x => x.GetClientByIdAsync(It.IsAny<long>()))
			                  .Returns(Task.FromResult<Client>(null));

			// Act
			var actionResult = await _clientsController.GetClientById(testClientId);

			// Assert
			actionResult.Should().BeOfType<NotFoundResult>();
		}

		[Fact]
		public async Task GetClientById_WhenClientDoesExist_ReturnOk()
		{
			// Arrange
			const int testClientId = 1;
			var clientToReturn = new Client { Id = testClientId };

			_clientServiceMock.Setup(x => x.GetClientByIdAsync(It.IsAny<long>()))
							  .Returns(Task.FromResult<Client>(clientToReturn));

			// Act
			var actionResult = await _clientsController.GetClientById(testClientId);

			// Assert
			actionResult.Should().BeOfType<OkObjectResult>();
		}

		[Fact]
		public async Task UpdateClient_WhenIdDoesNotMatchClient_ReturnsBadRequest()
		{
			// Arrange
			const int testClientId = 1;
			var testClient = new ClientDto { Id = 2 };

			// Act
			var actionResult = await _clientsController.UpdateClient(testClientId, testClient);

			// Assert
			actionResult.Should().BeOfType<BadRequestObjectResult>();
		}

		[Fact]
		public async Task UpdateClient_WhenIdMatchesClient_ButDoesNotExist_ReturnsNotFound()
		{
			// Arrange
			const int testClientId = 1;
			var testClient = new ClientDto { Id = testClientId };

			_clientServiceMock.Setup(x => x.ClientExistsAsync(It.IsAny<long>()))
			                  .Returns(Task.FromResult(false));

			// Act
			var actionResult = await _clientsController.UpdateClient(testClientId, testClient);

			// Assert
			actionResult.Should().BeOfType<NotFoundObjectResult>();
		}

		[Fact]
		public async Task UpdateClient_WhenSuccessful_ReturnsNoContent()
		{
			// Arrange
			const int testClientId = 1;
			var testClient = new ClientDto { Id = testClientId };

			_clientServiceMock.Setup(x => x.ClientExistsAsync(It.IsAny<long>()))
			                  .Returns(Task.FromResult(true));

			// Act
			var actionResult = await _clientsController.UpdateClient(testClientId, testClient);

			// Assert
			actionResult.Should().BeOfType<NoContentResult>();
		}

		[Fact]
		public async Task DeleteClient_WhenClientDoesNotExist_ReturnsNotFound()
		{
			// Arrange
			const int testClientId = 1;

			_clientServiceMock.Setup(x => x.GetClientByIdAsync(It.IsAny<long>()))
			                  .Returns(Task.FromResult<Client>(null));

			// Act
			var actionResult = await _clientsController.DeleteClient(testClientId);

			// Assert
			actionResult.Should().BeOfType<NotFoundObjectResult>();
		}

		[Fact]
		public async Task DeleteClient_WhenDoesExist_ReturnsNoContent()
		{
			// Arrange
			const int testClientId = 1;
			var testClient = new Client { Id = testClientId };

			_clientServiceMock.Setup(x => x.GetClientByIdAsync(It.IsAny<long>()))
			                  .Returns(Task.FromResult(testClient));

			// Act
			var actionResult = await _clientsController.DeleteClient(testClientId);

			// Assert
			actionResult.Should().BeOfType<NoContentResult>();
		}
	}
}
