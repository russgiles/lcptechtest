﻿using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Interfaces.Repositories;
using LcpTechTest.Core.Interfaces.Services;
using LcpTechTest.Core.Specifications;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LcpTechTest.Infrastructure.Services
{
	public class ClientService: IClientService
	{
		private readonly IGenericRepository<Client> _clientRepo;

		public ClientService(IGenericRepository<Client> clientRepo)
		{
			_clientRepo = clientRepo;
		}

		public async Task<IReadOnlyList<Client>> GetClients()
		{
			var spec = new ClientsWithNameSpecification();
			return await _clientRepo.ListAsync(spec);
		}

		public async Task<Client> GetClientByIdAsync(long clientId)
		{
			var spec = new ClientByIdSpecification(clientId);
			return await _clientRepo.GetEntityWithSpec(spec);
		}

		public async Task<bool> ClientExistsAsync(long clientId)
		{
			var spec = new ClientByIdSpecification(clientId);
			return await _clientRepo.ExistsAsync(spec);
		}

		public async Task<IReadOnlyList<Client>> SearchClientsAsync(string searchTerm)
		{
			var spec = new ClientsWithNameSpecification(searchTerm);
			return await _clientRepo.ListAsync(spec);
		}

		public async Task AddClientAsync(Client client)
		{
			client.Created = DateTime.Now;
			client.LastModified = DateTime.Now;
			await _clientRepo.AddAsync(client);
		}

		public async Task UpdateClientAsync(Client client)
		{
			client.LastModified = DateTime.Now;
			await _clientRepo.UpdateAsync(client);
		}

		public async Task DeleteClientAsync(Client client)
		{
			await _clientRepo.DeleteAsync(client);
		}
	}
}
