﻿using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Interfaces.Repositories;
using LcpTechTest.Core.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LcpTechTest.Infrastructure.Services
{
	public class ContactService : IContactService
	{
		private readonly IContactRepository _contactRepository;

		public ContactService(IContactRepository contactRepository)
		{
			_contactRepository = contactRepository;
		}

		public async Task<IEnumerable<Contact>> GetAllContactsAsync()
		{
			return await _contactRepository.GetAllContactsAsync();
		}

		public async Task<Contact> GetContactByIdAsync(long contactId)
		{
			return await _contactRepository.GetContactByIdAsync(contactId);
		}

		public async Task UpdateContactAsync(Contact contact)
		{
			contact.LastModified = DateTime.Now;
			await _contactRepository.UpdateContactAsync(contact);
		}

		public async Task AddContactAsync(Contact contact)
		{
			contact.Created = DateTime.Now;
			contact.LastModified = DateTime.Now;
			await _contactRepository.AddContactAsync(contact);
		}

		public async Task DeleteContactAsync(Contact contact)
		{
			await _contactRepository.DeleteContactAsync(contact);
		}

		public async Task<bool> ClientExistsAsync(long contactId)
		{
			return await _contactRepository.ContactExistsAsync(contactId);
		}
	}
}
