using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Interfaces.Repositories;
using LcpTechTest.Core.Specifications;
using LcpTechTest.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace LcpTechTest.Infrastructure.Repositories
{
	public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
	{
		private readonly LcpContext _context;

		public GenericRepository(LcpContext context)
		{
			_context = context;
		}

		public async Task<IReadOnlyList<T>> ListAllAsync()
		{
			return await _context.Set<T>().ToListAsync();
		}

		public async Task<T> GetEntityWithSpec(ISpecification<T> spec)
		{
			return await ApplySpecification(spec).FirstOrDefaultAsync();
		}

		public async Task<bool> ExistsAsync(ISpecification<T> spec)
		{
			return await ApplySpecification(spec).AnyAsync();
		}

		public async Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec)
		{
			return await ApplySpecification(spec).ToListAsync();
		}

		public async Task<int> CountAsync(ISpecification<T> spec)
		{
			return await ApplySpecification(spec).CountAsync();
		}

		public async Task AddAsync(T entity)
		{
			await _context.Set<T>().AddAsync(entity);
			await _context.SaveChangesAsync();
		}

		public async Task<int> UpdateAsync(T entity)
		{
			_context.Set<T>().Attach(entity);
			_context.Entry(entity).State = EntityState.Modified;
			return await _context.SaveChangesAsync();
		}

		public async Task<int> DeleteAsync(T entity)
		{
			_context.Set<T>().Remove(entity);
			return await _context.SaveChangesAsync();
		}

		private IQueryable<T> ApplySpecification(ISpecification<T> spec)
		{
			return SpecificationCreator<T>.GetQuery(_context.Set<T>().AsQueryable(), spec);
		}
	}
}
