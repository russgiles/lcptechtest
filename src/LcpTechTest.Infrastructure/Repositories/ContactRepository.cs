﻿using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Interfaces.Repositories;
using LcpTechTest.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LcpTechTest.Infrastructure.Repositories
{
	public class ContactRepository : IContactRepository
	{
		private readonly LcpContext _context;

		public ContactRepository(LcpContext context)
		{
			_context = context;
		}

		public async Task<IEnumerable<Contact>> GetAllContactsAsync()
		{
			return await _context.Contacts.ToListAsync();
		}

		public async Task<bool> ContactExistsAsync(long contactId)
		{
			return await _context.Contacts.AnyAsync(x => x.Id == contactId);
		}

		public async Task<Contact> GetContactByIdAsync(long contactId)
		{
			return await _context.Contacts.FindAsync(contactId);
		}

		public async Task UpdateContactAsync(Contact contact)
		{
			_context.Entry(contact).State = EntityState.Modified;
			await _context.SaveChangesAsync();
		}

		public async Task AddContactAsync(Contact contact)
		{
			await _context.Contacts.AddAsync(contact);
			await _context.SaveChangesAsync();
		}

		public async Task DeleteContactAsync(Contact contact)
		{
			_context.Contacts.Remove(contact);
			await _context.SaveChangesAsync();
		}
	}
}
