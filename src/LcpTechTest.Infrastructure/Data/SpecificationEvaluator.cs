using System.Linq;
using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Specifications;
using Microsoft.EntityFrameworkCore;

namespace LcpTechTest.Infrastructure.Data
{
	public class SpecificationCreator<TEntity> where TEntity : BaseEntity
	{
		public static IQueryable<TEntity> GetQuery(IQueryable<TEntity> inputQuery, ISpecification<TEntity> spec)
		{
			var query = inputQuery;

			if (spec.Criteria != null)
			{
				query = query.Where(spec.Criteria);
			}

			query = spec.Includes.Aggregate(query, (current, include) => current.Include(include));

			return query;
		}
	}
}
