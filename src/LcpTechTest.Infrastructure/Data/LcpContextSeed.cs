﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using LcpTechTest.Core.Entities;
using Microsoft.Extensions.Logging;

namespace LcpTechTest.Infrastructure.Data
{
	public class LcpContextSeed
	{
		public static async Task SeedAsync(LcpContext context, ILoggerFactory loggerFactory)
		{
			try
			{
				await context.Database.EnsureDeletedAsync();
				await context.Database.EnsureCreatedAsync();

				var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

				if (!context.Contacts.Any())
				{
					var jsonData = await GetData<Contact>($"{path}/Data/SeedData/contacts.json");

					foreach (var contact in jsonData)
					{
						UpdateDatetimeProps(contact);
						await context.Contacts.AddAsync(contact);
					}

					await context.SaveChangesAsync();
				}

				if (!context.ClientTypes.Any())
				{
					var jsonData = await GetData<ClientType>($"{path}/Data/SeedData/clientTypes.json");

					foreach (var clientType in jsonData)
					{
						UpdateDatetimeProps(clientType);
						await context.ClientTypes.AddAsync(clientType);
					}

					await context.SaveChangesAsync();
				}

				if (!context.Clients.Any())
				{
					var jsonData = await GetData<Client>($"{path}/Data/SeedData/clients.json");

					foreach (var client in jsonData)
					{
						UpdateDatetimeProps(client);
						await context.Clients.AddAsync(client);
					}

					await context.SaveChangesAsync();
				}
			}
			catch (Exception ex)
			{
				var logger = loggerFactory.CreateLogger<LcpContextSeed>();
				logger.LogError(ex.Message);
			}
		}

		private static async Task<List<T>> GetData<T>(string filePath)
		{
			var fileData = await File.ReadAllTextAsync(filePath);
			return JsonSerializer.Deserialize<List<T>>(fileData);
		}

		private static void UpdateDatetimeProps<T>(T obj) where T: BaseEntity
		{
			obj.Created = DateTime.Now;
			obj.LastModified = DateTime.Now;
		}
	}
}
