﻿using LcpTechTest.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace LcpTechTest.Infrastructure.Data
{
	public class LcpContext : DbContext
	{
		public LcpContext(DbContextOptions<LcpContext> options) : base(options)
		{
		}

		public DbSet<Client> Clients { get; set; }
		public DbSet<Contact> Contacts { get; set; }
		public DbSet<ClientType> ClientTypes { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ClientType>().ToTable("ClientTypes");
			modelBuilder.Entity<Contact>().ToTable("Contacts");
			modelBuilder.Entity<Client>().ToTable("Clients");

			AddClientRules(modelBuilder);
			AddContactRules(modelBuilder);
			AddClientTypeRules(modelBuilder);
		}

		private static void AddClientRules(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Client>()
						.Property(c => c.Id)
						.ValueGeneratedOnAdd()
						.IsRequired();

			modelBuilder.Entity<Client>()
						.Property(c => c.Name)
						.IsRequired()
						.HasMaxLength(20);

			modelBuilder.Entity<Client>()
						.HasOne(c => c.ClientType)
						.WithMany()
						.HasForeignKey(x => x.ClientTypeId)
						.IsRequired();

			modelBuilder.Entity<Client>()
						.HasOne(b => b.ClientType)
						.WithMany()
						.HasForeignKey(p => p.ClientTypeId);

			modelBuilder.Entity<Client>()
						.HasOne(b => b.CaseManager)
						.WithMany()
						.HasForeignKey(p => p.CaseManagerId);
		}

		private static void AddContactRules(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Contact>()
						.Property(c => c.Id)
						.ValueGeneratedOnAdd()
						.IsRequired();

			modelBuilder.Entity<Contact>()
						.Property(c => c.FirstName)
						.IsRequired()
						.HasMaxLength(20);

			modelBuilder.Entity<Contact>()
						.Property(c => c.Surname)
						.IsRequired()
						.HasMaxLength(20);

			modelBuilder.Entity<Contact>()
						.Property(c => c.Telephone)
						.IsRequired()
						.HasMaxLength(11);

			modelBuilder.Entity<Contact>()
						.Property(c => c.Email)
						.IsRequired()
						.HasMaxLength(200);
		}

		private static void AddClientTypeRules(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ClientType>()
			            .Property(c => c.Id)
			            .ValueGeneratedOnAdd()
			            .IsRequired();
		}
	}
}
