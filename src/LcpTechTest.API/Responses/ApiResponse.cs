namespace LcpTechTest.API.Responses
{
	public class ApiResponse
	{
		public ApiResponse(int statusCode, string message = null)
		{
			StatusCode = statusCode;
			Message = message ?? GetDefaultMessageForStatusCode(statusCode);
		}

		public int StatusCode { get; set; }
		public string Message { get; set; }

		private static string GetDefaultMessageForStatusCode(int statusCode)
		{
			return statusCode switch
			{
				400 => "Bad request was made!",
				404 => "Cannot find what you are looking for!",
				500 => "Oh no! Internal Server Error!",
				_ => null
			};
		}
	}
}
