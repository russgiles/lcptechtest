﻿using System;
using LcpTechTest.Core.Entities;
using LcpTechTest.Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LcpTechTest.API.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class ClientTypesController : ControllerBase
    {
        private readonly LcpContext _context;

        public ClientTypesController(LcpContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ClientType>>> GetClientTypes()
        {
            return await _context.ClientTypes.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ClientType>> GetClientType(long id)
        {
            var clientType = await _context.ClientTypes.FindAsync(id);

            if (clientType == null)
            {
                return NotFound();
            }

            return clientType;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutClientType(long id, ClientType clientType)
        {
            if (id != clientType.Id)
            {
                return BadRequest();
            }

            clientType.LastModified = DateTime.Now;
            _context.Entry(clientType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<ClientType>> PostClientType(ClientType clientType)
        {
	        clientType.Created= DateTime.Now;
	        clientType.LastModified = DateTime.Now;
            
	        await _context.ClientTypes.AddAsync(clientType);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetClientType", new { id = clientType.Id }, clientType);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClientType(long id)
        {
            var clientType = await _context.ClientTypes.FindAsync(id);
            if (clientType == null)
            {
                return NotFound();
            }

            _context.ClientTypes.Remove(clientType);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ClientTypeExists(long id)
        {
            return _context.ClientTypes.Any(e => e.Id == id);
        }
    }
}
