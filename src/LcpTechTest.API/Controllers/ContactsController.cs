﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LcpTechTest.API.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ContactsController : ControllerBase
	{
		private readonly IContactService _contactService;

		public ContactsController(IContactService contactService)
		{
			_contactService = contactService;
		}

		[HttpGet]
		public async Task<ActionResult<IEnumerable<Contact>>> GetContacts()
		{
			var results = await _contactService.GetAllContactsAsync();
			return Ok(results);
		}

		[HttpGet("{id}")]
		public async Task<ActionResult<Contact>> GetContact(long id)
		{
			var contact = await _contactService.GetContactByIdAsync(id);

			if (contact == null)
			{
				return NotFound();
			}

			return contact;
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> PutContact(long id, Contact contact)
		{
			if (id != contact.Id)
			{
				return BadRequest();
			}

			try
			{
				await _contactService.UpdateContactAsync(contact);
			}
			catch (DbUpdateConcurrencyException)
			{
				var contactExists = await _contactService.ClientExistsAsync(id);

				if (!contactExists)
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		[HttpPost]
		public async Task<ActionResult<Contact>> PostContact(Contact contact)
		{
			var contactExists = await _contactService.ClientExistsAsync(contact.Id);

			if (contactExists)
			{
				return BadRequest();
			}

			await _contactService.AddContactAsync(contact);

			return CreatedAtAction("GetContact", new { id = contact.Id }, contact);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteContact(long id)
		{
			var contact = await _contactService.GetContactByIdAsync(id);
			
			if (contact == null)
			{
				return NotFound();
			}

			await _contactService.DeleteContactAsync(contact);

			return NoContent();
		}
	}
}
