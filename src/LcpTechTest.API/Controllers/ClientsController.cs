﻿using AutoMapper;
using LcpTechTest.API.Dtos;
using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using LcpTechTest.API.Responses;

namespace LcpTechTest.API.Controllers
{
	[ApiController]
	[Route("api/[controller]")]
	public class ClientsController : ControllerBase
	{
		private readonly IClientService _clientService;
		private readonly IMapper _mapper;

		public ClientsController(IClientService clientService, IMapper mapper)
		{
			_clientService = clientService;
			_mapper = mapper;
		}

		[HttpGet]
		[ProducesResponseType(StatusCodes.Status200OK)]
		public async Task<ActionResult<IReadOnlyList<ClientToReturnDto>>> GetClients()
		{
			var clients = await _clientService.GetClients();
			var data = _mapper.Map<IReadOnlyList<Client>, IReadOnlyList<ClientToReturnDto>>(clients);

			return Ok(data);
		}

		[HttpGet]
		[Route("s/{searchTerm}")]
		public async Task<IEnumerable<Client>> SearchClients(string searchTerm)
		{
			return await _clientService.SearchClientsAsync(searchTerm);
		}

		[HttpGet("{id}")]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status200OK)]
		public async Task<IActionResult> GetClientById(long id)
		{
			var client = await _clientService.GetClientByIdAsync(id);

			if (client == null)
			{
				return NotFound();
			}

			return Ok(_mapper.Map<Client, ClientToReturnDto>(client));
		}


		[HttpPost]
		[ProducesResponseType(StatusCodes.Status201Created)]
		public async Task<ActionResult> CreateClient(ClientDto clientDto)
		{
			var client = _mapper.Map<ClientDto, Client>(clientDto);

			await _clientService.AddClientAsync(client);

			var newClient = await _clientService.GetClientByIdAsync(client.Id);

			var mappedNewClient = _mapper.Map<Client, ClientToReturnDto>(newClient);

			return CreatedAtAction(nameof(CreateClient), new { id = client.Id }, mappedNewClient);
		}

		[HttpPut]
		[Route("{id}")]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		public async Task<IActionResult> UpdateClient(long id, ClientDto client)
		{
			if (id != client.Id)
			{
				return BadRequest(new ApiResponse(400));
			}

			var clientExists = await _clientService.ClientExistsAsync(client.Id);

			if (!clientExists)
			{
				return NotFound(new ApiResponse(404));
			}

			var clientToUpdate = _mapper.Map<ClientDto, Client>(client);

			await _clientService.UpdateClientAsync(clientToUpdate);

			return NoContent();
		}

		[HttpDelete]
		[Route("{id}")]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status204NoContent)]
		public async Task<IActionResult> DeleteClient(long id)
		{
			var client = await _clientService.GetClientByIdAsync(id);

			if (client == null)
			{
				return NotFound(new ApiResponse(404));
			}

			await _clientService.DeleteClientAsync(client);
			
			return NoContent();
		}
	}
}
