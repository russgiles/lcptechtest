﻿namespace LcpTechTest.API.Dtos
{
	public class ClientDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int CaseManagerId { get; set; }
		public int ClientTypeId { get; set; }
	}
}
