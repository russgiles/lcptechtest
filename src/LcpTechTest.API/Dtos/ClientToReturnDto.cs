﻿namespace LcpTechTest.API.Dtos
{
	public class ClientToReturnDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string ClientType { get; set; }
		public string CaseManager { get; set; }
	}
}
