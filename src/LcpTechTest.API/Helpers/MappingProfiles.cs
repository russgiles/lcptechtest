﻿using AutoMapper;
using LcpTechTest.API.Dtos;
using LcpTechTest.Core.Entities;

namespace LcpTechTest.API.Helpers
{
	public class MappingProfiles : Profile
	{
		public MappingProfiles()
		{
			CreateMap<ClientDto, Client>();
			CreateMap<Client, ClientToReturnDto>()
				.ForMember(d => d.CaseManager, o => o.MapFrom(s => s.CaseManager.FullName))
				.ForMember(d => d.ClientType, o => o.MapFrom(s => s.ClientType.Name));
		}
	}
}
