﻿using LcpTechTest.Core.Interfaces.Repositories;
using LcpTechTest.Core.Interfaces.Services;
using LcpTechTest.Infrastructure.Repositories;
using LcpTechTest.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace LcpTechTest.API.Extensions
{
	public static class ApplicationServiceExtensions
	{
		public static IServiceCollection AddApplicationServices(this IServiceCollection services)
		{
			services.AddScoped<IClientService, ClientService>();
			services.AddScoped<IContactService, ContactService>();

			return services;
		}

		public static IServiceCollection AddApplicationRepositories(this IServiceCollection services)
		{
			services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
			services.AddScoped<IContactRepository, ContactRepository>();

			return services;
		}
	}
}
