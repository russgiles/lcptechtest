﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LcpTechTest.Core.Entities;

namespace LcpTechTest.Core.Interfaces.Services
{
	public interface IClientService
	{
		Task<IReadOnlyList<Client>> GetClients();
		Task<Client> GetClientByIdAsync(long clientId);
		Task<IReadOnlyList<Client>> SearchClientsAsync(string searchTerm);
		Task AddClientAsync(Client client);
		Task UpdateClientAsync(Client client);
		Task DeleteClientAsync(Client client);
		Task<bool> ClientExistsAsync(long clientId);
	}
}
