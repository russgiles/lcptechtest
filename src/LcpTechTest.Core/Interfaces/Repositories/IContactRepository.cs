﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LcpTechTest.Core.Entities;

namespace LcpTechTest.Core.Interfaces.Repositories
{
	public interface IContactRepository
	{
		Task<IEnumerable<Contact>> GetAllContactsAsync();
		Task<Contact> GetContactByIdAsync(long contactId);
		Task UpdateContactAsync(Contact contact);
		Task AddContactAsync(Contact contact);
		Task DeleteContactAsync(Contact contact);
		Task<bool> ContactExistsAsync(long contactId);
	}
}
