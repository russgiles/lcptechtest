using System.Collections.Generic;
using System.Threading.Tasks;
using LcpTechTest.Core.Entities;
using LcpTechTest.Core.Specifications;

namespace LcpTechTest.Core.Interfaces.Repositories
{
	public interface IGenericRepository<T> where T : BaseEntity
	{
		Task<IReadOnlyList<T>> ListAllAsync();
		Task<T> GetEntityWithSpec(ISpecification<T> spec);
		Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec);
		Task AddAsync(T entity);
		Task<int> UpdateAsync(T entity);
		Task<int> DeleteAsync(T entity);
		Task<bool> ExistsAsync(ISpecification<T> spec);
	}
}
