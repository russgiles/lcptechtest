﻿using System.ComponentModel.DataAnnotations;

namespace LcpTechTest.Core.Entities
{
	public class ClientType: BaseEntity
	{
		[Required]
		public string Name { get; set; }
	}
}
