﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LcpTechTest.Core.Entities
{
	public class Contact : BaseEntity
	{
		[Required, MaxLength(20)]
		public string FirstName { get; set; }
		[Required, MaxLength(20)]
		public string Surname { get; set; }
		[Required, StringLength(11)]
		public string Telephone { get; set; }
		[Required, EmailAddress, MaxLength(200)]
		public string Email { get; set; }
		[NotMapped]
		public string FullName => $"{FirstName} {Surname}";
	}
}
