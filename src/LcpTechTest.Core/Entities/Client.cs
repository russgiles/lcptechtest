﻿using System.ComponentModel.DataAnnotations;

namespace LcpTechTest.Core.Entities
{
	public class Client : BaseEntity
	{
		[Required, MaxLength(20)]
		public string Name { get; set; }
		public Contact CaseManager { get; set; }
		public long? CaseManagerId { get; set; }
		public ClientType ClientType { get; set; }
		[Required]
		public long ClientTypeId { get; set; }
	}
}
