﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LcpTechTest.Core.Entities
{
	public class BaseEntity
	{
		[Required]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }
		[Required]
		public DateTime Created { get; set; }
		[Required]
		public DateTime LastModified { get; set; }
	}
}
