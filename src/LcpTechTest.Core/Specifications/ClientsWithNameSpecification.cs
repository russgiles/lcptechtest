using LcpTechTest.Core.Entities;

namespace LcpTechTest.Core.Specifications
{
	public class ClientsWithNameSpecification : BaseSpecification<Client>
	{
		public ClientsWithNameSpecification()
		{
			AddInclude(o => o.ClientType);
			AddInclude(o => o.CaseManager);
		}

		public ClientsWithNameSpecification(string searchTerm) : base(x => string.IsNullOrEmpty(searchTerm) || x.Name.ToLower().Contains(searchTerm))
		{
			AddInclude(o => o.ClientType);
			AddInclude(o => o.CaseManager);
		}
	}
}
