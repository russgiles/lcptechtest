﻿using LcpTechTest.Core.Entities;

namespace LcpTechTest.Core.Specifications
{
	public class ClientByIdSpecification : BaseSpecification<Client>
	{
		public ClientByIdSpecification(long id): base(c => c.Id == id)
		{
			AddInclude(o => o.ClientType);
			AddInclude(o => o.CaseManager);
		}
}
}
